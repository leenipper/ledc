package main // import "gitlab.com/leenipper/ledc/client"

import (
	"log"

	"gitlab.com/leenipper/ledc/ledi"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var conn *grpc.ClientConn

	conn, err := grpc.Dial(":7777", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()

	c := ledi.NewLedOpClient(conn)

	response, err := c.Put(context.Background(), &ledi.LedReq{Port: "2", Mode: ledi.Mode_ON})
	if err != nil {
		log.Fatalf("Error when calling Put: %s", err)
	}
	log.Printf("Response from server: %s %s", response.Port, response.Mode)
	response, err = c.Put(context.Background(), &ledi.LedReq{Port: "2", Mode: ledi.Mode_OFF})
	if err != nil {
		log.Fatalf("Error when calling Put: %s", err)
	}
	log.Printf("Response from server: %s %s", response.Port, response.Mode)
}
