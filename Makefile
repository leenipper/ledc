SERVER_OUT := server.bin
CLIENT_OUT := client.bin
SERVICE := ledi
SERVICE_PB_GEN_SRC := $(SERVICE)/$(SERVICE).pb.go

.PHONY: all $(SERVICE) build_server build_client

all: build_server build_client

$(SERVICE)/$(SERVICE).pb.go: $(SERVICE)/$(SERVICE).proto
	protoc -I $(SERVICE)/ \
		-I$(GOPATH)/src \
		--go_out=plugins=grpc:$(SERVICE) \
		$(SERVICE)/$(SERVICE).proto

$(SERVICE): $(SERVICE)/$(SERVICE).pb.go ## Auto-generate grpc go sources

dep: ## Get the dependencies
	go get -v -d ./...

build_server: dep $(SERVICE) ## Build the binary file for server
	cd server && go build -i -v -o $(SERVER_OUT)

build_client: dep $(SERVICE) ## Build the binary file for client
	cd client && go build -i -v -o $(CLIENT_OUT)

clean: ## Remove previous builds
	rm -f server/$(SERVER_OUT) client/$(CLIENT_OUT) $(SERVICE_PB_GEN_SRC)
