// Code generated by protoc-gen-go. DO NOT EDIT.
// source: ledi.proto

package ledi

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Mode int32

const (
	Mode_OFF        Mode = 0
	Mode_ON         Mode = 1
	Mode_SLOW_FLASH Mode = 2
	Mode_FAST_FLASH Mode = 3
)

var Mode_name = map[int32]string{
	0: "OFF",
	1: "ON",
	2: "SLOW_FLASH",
	3: "FAST_FLASH",
}

var Mode_value = map[string]int32{
	"OFF":        0,
	"ON":         1,
	"SLOW_FLASH": 2,
	"FAST_FLASH": 3,
}

func (x Mode) String() string {
	return proto.EnumName(Mode_name, int32(x))
}

func (Mode) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_9c603c5f4aecf8d3, []int{0}
}

type LedReq struct {
	Port                 string   `protobuf:"bytes,1,opt,name=port,proto3" json:"port,omitempty"`
	Mode                 Mode     `protobuf:"varint,2,opt,name=mode,proto3,enum=ledi.Mode" json:"mode,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *LedReq) Reset()         { *m = LedReq{} }
func (m *LedReq) String() string { return proto.CompactTextString(m) }
func (*LedReq) ProtoMessage()    {}
func (*LedReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_9c603c5f4aecf8d3, []int{0}
}

func (m *LedReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_LedReq.Unmarshal(m, b)
}
func (m *LedReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_LedReq.Marshal(b, m, deterministic)
}
func (m *LedReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_LedReq.Merge(m, src)
}
func (m *LedReq) XXX_Size() int {
	return xxx_messageInfo_LedReq.Size(m)
}
func (m *LedReq) XXX_DiscardUnknown() {
	xxx_messageInfo_LedReq.DiscardUnknown(m)
}

var xxx_messageInfo_LedReq proto.InternalMessageInfo

func (m *LedReq) GetPort() string {
	if m != nil {
		return m.Port
	}
	return ""
}

func (m *LedReq) GetMode() Mode {
	if m != nil {
		return m.Mode
	}
	return Mode_OFF
}

type LedResp struct {
	Port                 string   `protobuf:"bytes,1,opt,name=port,proto3" json:"port,omitempty"`
	Mode                 Mode     `protobuf:"varint,2,opt,name=mode,proto3,enum=ledi.Mode" json:"mode,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *LedResp) Reset()         { *m = LedResp{} }
func (m *LedResp) String() string { return proto.CompactTextString(m) }
func (*LedResp) ProtoMessage()    {}
func (*LedResp) Descriptor() ([]byte, []int) {
	return fileDescriptor_9c603c5f4aecf8d3, []int{1}
}

func (m *LedResp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_LedResp.Unmarshal(m, b)
}
func (m *LedResp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_LedResp.Marshal(b, m, deterministic)
}
func (m *LedResp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_LedResp.Merge(m, src)
}
func (m *LedResp) XXX_Size() int {
	return xxx_messageInfo_LedResp.Size(m)
}
func (m *LedResp) XXX_DiscardUnknown() {
	xxx_messageInfo_LedResp.DiscardUnknown(m)
}

var xxx_messageInfo_LedResp proto.InternalMessageInfo

func (m *LedResp) GetPort() string {
	if m != nil {
		return m.Port
	}
	return ""
}

func (m *LedResp) GetMode() Mode {
	if m != nil {
		return m.Mode
	}
	return Mode_OFF
}

func init() {
	proto.RegisterEnum("ledi.Mode", Mode_name, Mode_value)
	proto.RegisterType((*LedReq)(nil), "ledi.LedReq")
	proto.RegisterType((*LedResp)(nil), "ledi.LedResp")
}

func init() { proto.RegisterFile("ledi.proto", fileDescriptor_9c603c5f4aecf8d3) }

var fileDescriptor_9c603c5f4aecf8d3 = []byte{
	// 186 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xca, 0x49, 0x4d, 0xc9,
	0xd4, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x01, 0xb1, 0x95, 0x6c, 0xb8, 0xd8, 0x7c, 0x52,
	0x53, 0x82, 0x52, 0x0b, 0x85, 0x84, 0xb8, 0x58, 0x0a, 0xf2, 0x8b, 0x4a, 0x24, 0x18, 0x15, 0x18,
	0x35, 0x38, 0x83, 0xc0, 0x6c, 0x21, 0x39, 0x2e, 0x96, 0xdc, 0xfc, 0x94, 0x54, 0x09, 0x26, 0x05,
	0x46, 0x0d, 0x3e, 0x23, 0x2e, 0x3d, 0xb0, 0x76, 0xdf, 0xfc, 0x94, 0xd4, 0x20, 0xb0, 0xb8, 0x92,
	0x2d, 0x17, 0x3b, 0x58, 0x77, 0x71, 0x01, 0x39, 0xda, 0xb5, 0xcc, 0xb9, 0x58, 0x40, 0x3c, 0x21,
	0x76, 0x2e, 0x66, 0x7f, 0x37, 0x37, 0x01, 0x06, 0x21, 0x36, 0x2e, 0x26, 0x7f, 0x3f, 0x01, 0x46,
	0x21, 0x3e, 0x2e, 0xae, 0x60, 0x1f, 0xff, 0xf0, 0x78, 0x37, 0x1f, 0xc7, 0x60, 0x0f, 0x01, 0x26,
	0x10, 0xdf, 0xcd, 0x31, 0x38, 0x04, 0xca, 0x67, 0x36, 0xd2, 0xe5, 0x62, 0xf5, 0x49, 0x4d, 0xf1,
	0x2f, 0x10, 0x52, 0xe1, 0x62, 0x0e, 0x28, 0x2d, 0x11, 0xe2, 0x81, 0x18, 0x0d, 0xf1, 0x89, 0x14,
	0x2f, 0x12, 0xaf, 0xb8, 0x40, 0x89, 0x21, 0x89, 0x0d, 0xec, 0x63, 0x63, 0x40, 0x00, 0x00, 0x00,
	0xff, 0xff, 0x6f, 0xd1, 0xbc, 0x44, 0xff, 0x00, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// LedOpClient is the client API for LedOp service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type LedOpClient interface {
	Put(ctx context.Context, in *LedReq, opts ...grpc.CallOption) (*LedResp, error)
}

type ledOpClient struct {
	cc *grpc.ClientConn
}

func NewLedOpClient(cc *grpc.ClientConn) LedOpClient {
	return &ledOpClient{cc}
}

func (c *ledOpClient) Put(ctx context.Context, in *LedReq, opts ...grpc.CallOption) (*LedResp, error) {
	out := new(LedResp)
	err := c.cc.Invoke(ctx, "/ledi.LedOp/Put", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// LedOpServer is the server API for LedOp service.
type LedOpServer interface {
	Put(context.Context, *LedReq) (*LedResp, error)
}

// UnimplementedLedOpServer can be embedded to have forward compatible implementations.
type UnimplementedLedOpServer struct {
}

func (*UnimplementedLedOpServer) Put(ctx context.Context, req *LedReq) (*LedResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Put not implemented")
}

func RegisterLedOpServer(s *grpc.Server, srv LedOpServer) {
	s.RegisterService(&_LedOp_serviceDesc, srv)
}

func _LedOp_Put_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(LedReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(LedOpServer).Put(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/ledi.LedOp/Put",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(LedOpServer).Put(ctx, req.(*LedReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _LedOp_serviceDesc = grpc.ServiceDesc{
	ServiceName: "ledi.LedOp",
	HandlerType: (*LedOpServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Put",
			Handler:    _LedOp_Put_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "ledi.proto",
}
