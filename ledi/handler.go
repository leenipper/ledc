package ledi

import (
	"log"

	"golang.org/x/net/context"
)

// Server represents the gRPC server
type Server struct {
}

// Put sets the mode for led for given port replying with LedResp
func (s *Server) Put(ctx context.Context, in *LedReq) (*LedResp, error) {
	log.Printf("ledi Put LedReq port %s mode %s", in.Port, in.Mode)
	return &LedResp{Port: in.Port, Mode: in.Mode}, nil
}
